<?php

namespace App\Nova;

use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Currency;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;

class Order extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Order';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Currency::make('Сумма', 'sum'),

            Text::make('Продукты', function (){
                $html = '<table><tr><td class="text-center"><b>Наименование</b></td><td  style="text-align: center; width: 5em"><b>Количество</b></td>  <td  style="width: 6em; text-align: right"><b>Цена</b></td><td style="width: 8em; text-align: right"><b>Сумма</b></td></tr>';

                foreach ($this->products as $product){
                    $categoryName = $product->category->name;
                    $productName = $product->name;
                    $quantity = $product->pivot->quantity;
                    $salePrice = $product->pivot->sale_price;
                    $sum = (float)((float)$salePrice * (int)$quantity);
                    $html .= "<tr><td>$categoryName $productName</td><td class=\"text-center\">$quantity</td>  <td class=\"text-right\">$salePrice</td><td class=\"text-right\">$sum сом</td></tr>";
                }
                $html .= '</<table>';

                return $html;
            })->asHtml()->onlyOnDetail(),

            Text::make('Продукты', function (){
                return $this->products->count();
            })->onlyOnIndex(),

            Boolean::make('Оплачено', 'paid')
        ];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        return $query->with('products.category');
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public static function label()
    {
        return 'Продажи';
    }

    public static function singularLabel()
    {
        return ['Продажа', 'продажу', 'продажи', 'а'];
    }
}
