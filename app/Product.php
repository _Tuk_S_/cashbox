<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{

    protected $table = 'products';

    protected $fillable = ['name', 'purchase_price', 'sale_price', 'qty', 'category_id'];


    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function orders() {
        return $this->belongsToMany(
            Product::class,
            'order_products',
            'product_id',
            'order_id'
        )->withPivot('quantity', 'sale_price');
    }
}

