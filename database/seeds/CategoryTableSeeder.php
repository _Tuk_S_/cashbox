<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shops = [
            [
                'name' => 'Масло',
                'children' => [
                    [
                        'name' => 'Растительное',
                        'children' => [
                            ['name' => 'Оведов'],
                            ['name' => 'Шедевр'],
                            ['name' => 'Багатовская'],
                        ],
                    ],
                    [
                        'name' => 'Сливочное',
                        'children' => [
                            ['name' => 'Крестьянское'],
                            ['name' => 'Бутербродная'],
                            ['name' => 'Особая'],
                        ],
                    ],
                ],
            ],
            [
                'name' => 'Мука',
                'children' => [
                    [
                        'name' => 'Акун',
                        'children' => [
                            ['name' => 'Дасторкон 1-сорт'],
                            ['name' => 'Дасторкон высший-сорт'],
                        ],
                    ],
                    [
                        'name' => 'Пионер',
                        'children' => [
                            ['name' => 'Пионер 1-сорт'],
                            ['name' => 'Пионер высший'],
                        ],
                    ],
                ],
            ],
        ];
        foreach($shops as $shop)
        {
            \App\Category::create($shop);
        }
    }
}
