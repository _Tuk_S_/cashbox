<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $shops = [
            [
                'name' =>  '5л',
                'purchase_price' => 390,
                'sale_price' => 400,
                'qty' => 80,
                'category_id' => 3,
            ],
            [
                'name' =>  '2л',
                'purchase_price' => 156,
                'sale_price' => 160,
                'qty' => 80,
                'category_id' => 3,
            ],
            [
                'name' =>  '1л',
                'purchase_price' => 78,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 3,
            ],


            [
                'name' =>  '5л',
                'purchase_price' => 350,
                'sale_price' => 380,
                'qty' => 80,
                'category_id' => 4,
            ],

            [
                'name' =>  '1л',
                'purchase_price' => 70,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 4,
            ],



            [
                'name' =>  '5л',
                'purchase_price' => 270,
                'sale_price' => 300,
                'qty' => 80,
                'category_id' => 5,
            ],
            [
                'name' =>  '1л',
                'purchase_price' => 61,
                'sale_price' => 65,
                'qty' => 80,
                'category_id' => 5,
            ],


            [
                'name' =>  '200гр',
                'purchase_price' => 78,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 7,
            ],


            [
                'name' =>  '200гр',
                'purchase_price' => 30,
                'sale_price' => 35,
                'qty' => 80,
                'category_id' => 8,
            ],


            [
                'name' =>  '200гр',
                'purchase_price' => 20,
                'sale_price' => 25,
                'qty' => 80,
                'category_id' => 9,
            ],


            [
                'name' =>  '50кг',
                'purchase_price' => 1065,
                'sale_price' => 1150,
                'qty' => 80,
                'category_id' => 12,
            ],


            [
                'name' =>  '25кг',
                'purchase_price' => 580,
                'sale_price' => 600,
                'qty' => 80,
                'category_id' => 12,
            ],

            [
                'name' =>  '10кг',
                'purchase_price' => 250,
                'sale_price' => 280,
                'qty' => 80,
                'category_id' => 12,
            ],

            [
                'name' =>  '5кг',
                'purchase_price' => 130,
                'sale_price' => 150,
                'qty' => 80,
                'category_id' => 12,
            ],

            [
                'name' =>  '2кг',
                'purchase_price' => 67,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 12,
            ],




            [
                'name' =>  '50кг',
                'purchase_price' => 78,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 13,
            ],


            [
                'name' =>  '10кг',
                'purchase_price' => 78,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 13,
            ],

            [
                'name' =>  '5кг',
                'purchase_price' => 78,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 13,
            ],

            [
                'name' =>  '2кг',
                'purchase_price' => 78,
                'sale_price' => 80,
                'qty' => 80,
                'category_id' => 13,
            ],




            [
                'name' =>  '50кг',
                'purchase_price' => 1080,
                'sale_price' => 1200,
                'qty' => 80,
                'category_id' => 15,
            ],


            [
                'name' =>  '10кг',
                'purchase_price' => 270,
                'sale_price' => 300,
                'qty' => 80,
                'category_id' => 15,
            ],

            [
                'name' =>  '5кг',
                'purchase_price' => 150,
                'sale_price' => 160,
                'qty' => 80,
                'category_id' => 15,
            ],




            [
                'name' =>  '50кг',
                'purchase_price' => 1200,
                'sale_price' => 1350,
                'qty' => 80,
                'category_id' => 16,
            ],


            [
                'name' =>  '10кг',
                'purchase_price' => 310,
                'sale_price' => 330,
                'qty' => 80,
                'category_id' => 16,
            ],

            [
                'name' =>  '5кг',
                'purchase_price' => 170,
                'sale_price' => 200,
                'qty' => 80,
                'category_id' => 16,
            ],

        ];

        foreach ($shops as $shop) {
            \App\Product::create($shop);
        }
    }
}
