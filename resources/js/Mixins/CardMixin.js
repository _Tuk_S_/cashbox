export default {
    data(){
        return {
            category: {
                products: []
            },
            basket: [],
        }
    },
    computed: {
        sum() {
            let sum = 0;
            this.basket.forEach(item => {
                sum += parseFloat(item.product.sale_price) * parseInt(item.quantity);
            })
            return sum
        }
    },

    methods:{
        addCard(data) {
            let exists = false;
            this.basket.forEach(item => {
                if (item.productId === data.product.id) {
                    item.quantity = parseInt(item.quantity) + parseInt(data.quantity);
                    exists = true
                }
                return item.productId === data.product.id
            });


            if (exists === false) {
                this.basket.push({
                    category: {name: data.category.name},
                    product : data.product,
                    productId: data.product.id,
                    quantity: data.quantity
                })
            }

        },

        removeCard(index) {
            this.basket.splice(index, 1);
        }
    }
}
