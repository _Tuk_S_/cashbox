<div style="margin: 10px 40px">
    <div style="text-align: center">
        <h2>"Береке"</h2>

        <p><b>Товарный чек № {{$order->id}}</b></p>

        <p><i>телефон склада: +996 702 305 002</i></p>
        <p><i>дата покупки: {{$order->created_at}}</i></p>


    </div>

    <table style="width: 100%">
        <thead>
        <tr>
            <td><b>#</b></td>
            <td><b>Наименование</b></td>
            <td><b>Количество</b></td>
            <td><b>Цена</b></td>
            <td><b>Сумма</b></td>
        </tr>
        </thead>
        <tbody>

        @foreach($order->products as $product)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$product->category->name}} {{$product->name}}</td>
                <td>{{$product->pivot->quantity}}</td>
                <td>{{$product->pivot->sale_price}} сом</td>
                <td>{{(float)$product->pivot->sale_price * (float)$product->pivot->quantity}} сом</td>
            </tr>

        @endforeach
        </tbody>
    </table>

    <h4 style="text-align: right; margin-right: 10%">Итого: {{$order->sum}} сом</h4>
    <h3 style="text-align: center">Спасибо за покупку!!!</h3>

</div>





