<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="name">
    <meta name="description" content="description here">
    <meta name="keywords" content="keywords,here">
    <link href="/css/tailwind.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
</head>

<body class="font-sans leading-normal tracking-normal mt-12">


<div id="app">
    <div>
        <example-component></example-component>

        {{--<ul>--}}
        {{--@foreach($tree as $category1)--}}
        {{--<li>--}}
        {{--{{ $category1->name }} ({{$category1->products->count()}})--}}
        {{--</li>--}}
        {{--<ul>--}}
        {{--@foreach($category1->children as $category2)--}}
        {{--<li>--}}
        {{--{{$category2->name}} ({{$category2->products->count()}})--}}
        {{--</li>--}}
        {{--<ul>--}}
        {{--@foreach($category2->children as $category3)--}}
        {{--<li>--}}
        {{--{{$category3->name}} ({{$category3->products->count()}})--}}
        {{--</li>--}}
        {{--@endforeach--}}
        {{--</ul>--}}
        {{--@endforeach--}}
        {{--</ul>--}}
        {{--@endforeach--}}
        {{--</ul>--}}
    </div>

</div>



<script src="/js/app.js"></script>

</body>

</html>




