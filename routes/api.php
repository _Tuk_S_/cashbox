<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/categories-tree', function () {
    $tree = \App\Category::with('products')->get()->toTree();
    return response()->json($tree->toArray());
});


Route::post('/create-order', function (Request $request) {

    $sum = $request->get('sum');
    $order = \App\Order::create([
        'sum' => $sum,
        'paid' => $request->get('paid'),
    ]);

    $data = [];
    foreach ($request->all()['basket'] as $value) {
        $data[$value['product']['id']] = ['quantity' => (int)$value['quantity'], 'sale_price' => (float)$value['product']['sale_price']];
    }

    if ($order) {
        $order->products()->attach($data);
    }

    $order = \App\Order::with('products.category')->find($order->id);

    $html = view('receipt', ['order' => $order] )->render();

    return response()->json([
        'result' => $order,
        'html' => $html,
    ]);

});
